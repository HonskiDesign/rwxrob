## Timestamps for RWXRob's Linux Beginner Boost Series

## Day 1

[Welcome and Session Time Breakdown](https://youtu.be/CI-FE2bKr7c?t=132)\
[Discord Discussion](https://youtu.be/CI-FE2bKr7c?t=283)


##### Segment 1

[Session Start](https://youtu.be/CI-FE2bKr7c?t=733)\
[Rob Introduces Himself](https://youtu.be/CI-FE2bKr7c?t=871)\
[What is the Beginner Boost](https://youtu.be/CI-FE2bKr7c?t=969)\
[What will you learn?](https://youtu.be/CI-FE2bKr7c?t=1079)

##### Course Overview

[Prerequisites](https://youtu.be/CI-FE2bKr7c?t=1325)\
[Not like other courses](https://youtu.be/CI-FE2bKr7c?t=1972)\
[Target Occupations](https://youtu.be/CI-FE2bKr7c?t=2165)\
[Weekly Topics](https://youtu.be/CI-FE2bKr7c?t=2402)\
[Stats and Numbers](https://youtu.be/CI-FE2bKr7c?t=2402)\
[Getting Started](https://youtu.be/CI-FE2bKr7c?t=3094)\
[Goals](https://youtu.be/CI-FE2bKr7c?t=3130)\
[RWXs Wife](https://youtu.be/CI-FE2bKr7c?t=3254)

##### Segment 2

[Open Course](https://youtu.be/CI-FE2bKr7c?t=4512)\
[Open Credentials](https://youtu.be/CI-FE2bKr7c?t=4624)\
[Course Logistics](https://youtu.be/CI-FE2bKr7c?t=5080)\
[Rules](https://youtu.be/CI-FE2bKr7c?t=5399)\
[Learning to Learn](https://youtu.be/CI-FE2bKr7c?t=5624)\
[Understanding Web Browsers](https://youtu.be/CI-FE2bKr7c?t=5689)\
[Setup Protonmail](https://youtu.be/CI-FE2bKr7c?t=6467)

##### Segment 3

[The Problem with Repl.it](https://youtu.be/CI-FE2bKr7c?t=8101)\
[Repl.it, Getting Going](https://youtu.be/CI-FE2bKr7c?t=8357)\
[Repl.it, Bash](https://youtu.be/CI-FE2bKr7c?t=8434)\
[Repl.it, Web-HTML, CSS, JS](https://youtu.be/CI-FE2bKr7c?t=9309)\
[Repl.it, Node.js](https://youtu.be/CI-FE2bKr7c?t=10385)\
[Repl.it, C](https://youtu.be/CI-FE2bKr7c?t=10672)\
[Repl.it, Go](https://youtu.be/CI-FE2bKr7c?t=10885)\
[Repl,it, Esoteric Languages](https://youtu.be/CI-FE2bKr7c?t=10885)
